# Terraform for GitLab groups, projects, and access.
# https://www.terraform.io/docs/providers/gitlab/r/project.html#default_branch
#
# Terraform CLI is FOSS:
# https://github.com/hashicorp/terraform


# The Demo will walk you through the creation of a gitlab group and repository.
# Once the group is created, we'll add a new user to group using data from GitLab.
#
# Download the terraform client. https://www.terraform.io/downloads.html
# I put mine in ~/bin/ to make life easy.


# Terraform uses "providers" to interact with an applications api.
# In line 24-27 is the GitLab Provider.
#
# Create an access token in GitLab, and paste it between the "" on line 26.
# https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token

#provider "gitlab" {
#    token     =  ""
#}

# In a terminal, navigate to the "terraform-demo" directory and type "terraform init".
# This will configure the working directory for Terraform.
#
# Terraform "providers" use "resources" to configure specific parts of applications.
#
# Uncomment lines 38-44
# On line 38, name the resource in Terraform by typing a name between the "".
#
# These name spaces can be called as variables in other Terraform resources.
# See the "data" section of a resource (line 2).
#
# Next give the new gitlab_group a "name" and "path".

#resource "gitlab_group" "" {
#  name        = ""
#  path        = ""
#  description = "terraform demo."
#  request_access_enabled = "false"
#  visibility_level = "private"
#}

# Run "terraform plan", Terraform creates a map for what actions it will take.
# Run "terraform apply", answer "yes", Terraform will apply the map creating the group.


# Next we'll create a project using the "gitlab_project" resource.
#
# Uncomment lines 60-73
# Name the resource on line 60, nd add a name for the project on line 61.
#
# for "namespaceid =" we want to call the gitlab_group we created previously.
# We call the resource "gitlab_group.resource_name" and we can get the namespaceid, by adding .id to the end.
#
# Example: gitlab_group.demo_group.id  -  (this does not get put in "")

#resource "gitlab_project" "" {
#  name = "-repo"
#  namespace_id =
#  description  = "A demo project"
#  default_branch = "master"
#  visibility_level = "private"
#  request_access_enabled = "true"
#  merge_requests_enabled = "true"
#  merge_method = "rebase_merge"
#  only_allow_merge_if_all_discussions_are_resolved = "true"
#  lfs_enabled = "true"
#  archived = "false"
#  initialize_with_readme = "false"
#}

# Run "terraform plan" it'll show that the project is going to be created.
# Run "terraform apply" to make the changes.


# We can use data from GitLab to grant a user access to the group.
#
# Uncomment 87-89
# Name the resource on line 89.
#
# Add a GitLab username between the ""'s on line 83.
# Feel free to use my username: nicholasburr

#data "gitlab_user" "" {
#  username = ""
#}

# Uncomment 95-99
# Name the resource on line 95.
#
# Call the group we created earlier
# example: gitlab_group.gitlab_group_resource_name.id
#
# Call the data for "user_id"
# example: data.gitlab_user.data_resource_name.id

#resource "gitlab_group_membership" "" {
#  group_id     =
#  user_id      =
#  access_level = "developer"
#}

# Run "terraform plan", verify your changes.
# Run "terraform apply" to make the changes.


# Now play around with making changes to the infrastructure.
# For an easy change, on line 63 change "private" to "public"
#
# Run "terraform plan", verify your changes.
# Note that Terraform knows it's going to change the state of a resource.
#
# Run "terraform apply" to make the changes.
